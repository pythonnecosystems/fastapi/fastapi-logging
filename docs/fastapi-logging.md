# FastAPI 로깅 2023<sup>[1](#footnote_1)</sup>
백엔드 소프트웨어 개발의 세계에서 적절한 로깅은 모든 어플리케이션 프로그래밍 인터페이스(API)의 필수적인 부분이다. API에서 항상 무슨 일이 일어나고 있는지 확인할 수 있으면 문제를 쉽게 디버깅하고 클라이언트가 API와 상호 작용하는 방식을 확인하며 웹사이트의 전반적인 상태를 주시할 수 있다. 그래서 FastAPI에서 로깅을 구현하는 몇 가지 방법을 살펴본다.

## 콘솔 로깅
가장 쉽게 구현할 수 있는 로깅 타입은 콘솔 로깅이다. 가장 간단한 형태로, Python을 사용할 때 이렇게만 하면 된다.

```python
print("Some log message.")
```

이제 이것이 쉽고 멋져 보일지 모르지만, 이 `print` 함수는 문자열을 표준 출력(stdout)으로 인쇄하는 것뿐이다. 적절한 로깅을 위해서는 좀 더 강력한 무언가가 필요하다. `Debug`, `Critical`, `Error`, `Info` 등 다양한 로깅 수준을 모두 적절히 활용할 수 있는 무언가가 필요하다. 다행히도 이러한 요구를 충족시킬 수 있는 것이 있다. 바로 Python 로깅 모듈이다. 이 모듈은 Python의 표준 라이브러리의 일부이지만, 어떤 이유로든 개발 환경에서 누락된 경우 `pip`를 사용하여 설치할 수 있다.

```bash
$ pip install logging
```

설치가 완료되면 Tiangolo의 FastAPI 튜토리얼에서 이 코드 스니팻을 사용하여 새 FastAPI 프로젝트를 생성한다.

```python
from fastapi import FastAPI

app = FastAPI()

@app.get("/")
async def root():
    return {"message": "Hello World"}
```

다음 파일 상단에 가져오기를 추가하여 새로 설치한 모듈을 활용하고자 한다. 또한 Fastapi 모듈에서 응답과 요청 함수를 가져오고 싶다.

```python
from fastapi import FastAPI, Response, Request
import logging
```

그런 다음 FastAPI 초기화 직후 기본 구성을 만들고 루트 로깅 수준을 `DEBUG`로 설정한다. 그리고 `getLogger` 함수를 사용하여 `logger` 인스턴스에 대한 참조를 생성한다. 프로젝트에서 여러 모듈(파일)을 사용하는 경우 유용하게 사용할 수 있다.

```python
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
```

이 시점에서 남은 일은 로깅을 시작하는 것뿐이다. 따라서 API 사용자에게 전송되는 각 API 요청과 응답을 캡처하는 미들웨어를 작성할 것이다. 그 모습은 다음과 같다.

```python
@app.middleware("http")
async def api_logging(request: Request, call_next):
    response = await call_next(request)

    response_body = b""
    async for chunk in response.body_iterator:
        response_body += chunk
    log_message = {
        "host": request.url.hostname,
        "endpoint": request.url.path,
        "response": response_body.decode()
    }
    logger.debug(log_message)
    return Response(content=response_body, status_code=response.status_code, 
        headers=dict(response.headers), media_type=response.media_type)
```

이 코드의 기능을 자세히 살펴보면, 먼저 FastAPI 미들웨어 데코레이터로 비동기 함수를 생성하는 것으로 시작한다. 함수 자체는 두 개의 인수를 받는데, 첫 번째 인수는 API로 전송되는 `request`이고 두 번째 인수는 `call_next` 함수에 대한 참조이다. `call_next` 함수는 요청을 지정된 경로로 전달한 다음 해당 경로에서 응답을 반환한다. 함수 본문에서는 `call_next` 함수를 사용하여 API 응답을 가져오고 이를 반복하여 바이트열 객체 형태로 저장된 응답 본문을 가져온다. 그런 다음 호스트, 엔드포인트 및 API 응답이 포함된 커스텀 딕셔너리을 사용하여 메시지를 최종적으로 기록할 수 있다. 미들웨어 함수에서 마지막으로 해야 할 일은 응답(response)을 반환하는 것이다. 이 부분에서는 `response` 함수를 사용하는데, 이는 고도로 커스텀 가능한 응답을 반환할 수 있기 때문이다.

이제 남은 일은 모든 것을 테스트하는 것이다. 이를 위해 프로젝트 디렉토리에서 다음 콘솔 명령을 실행하여 API를 시작한다.

```bash
$ python3 -m uvicorn main:app --host 0.0.0.0 --port 8000 --reload
```

모든 것이 실행 중이고 성가신 오류가 나타나지 않으면 Postman이나 웹 브라우저를 사용하여 요청을 보내보자. 응답에 다음과 같은 내용이 표시될 것이다.

![](./images/1_vIHSIoo1uGbgtekjMqA1Og.webp)

콘솔로 돌아가면 미들웨어 기능이 메시지를 기록한 것을 확인할 수 있을 것이다.

## 파일 로깅
콘솔 로깅도 훌륭하지만, 큰 어플리케이션의 경우 보다 강력한 로깅 방법이 필요하다. 다시 한 번, Python의 로깅 모듈은 콘솔 대신 파일에 로그를 인쇄할 수 있는 기능을 제공하여 도움을 줄 수 있다. 파일에 로깅을 시작하려면 이전 섹션에서 호출한 기본 함수를 수정하기만 하면 된다.

```python
logging.basicConfig(
    filename="api_log.txt",
    filemode="a",
    level=logging.DEBUG
)
```

파일 이름을 지정하고, 이 경우 추가할 파일 모드를 선택한 다음 마지막으로 로깅 수준을 설정하기만 하면 된다. 그런 다음 웹 브라우저나 Postman을 사용하여 다른 요청을 보내본다. 이제 프로젝트 디렉토리에 `api_log.txt` 파일이 나타난다. 이 파일을 열면 로깅 모듈이 API와 주고받는 각 요청과 응답을 캡처한 것을 볼 수 있다.

## 데이터베이스 로깅
마지막으로 살펴볼 타입은 데이터베이스 로깅이다. 이러한 종류의 로깅은 로그 레코드를 쉽게 쿼리할 수 있는 방법이 필요한 경우 유용할 수 있다. 이제 데이터베이스 로깅을 몇 가지 다른 방법으로 구현할 수 있지만, 이전에 수행한 작업을 기반으로 로깅 요청을 처리하는 두 번째 API를 만들어 보겠다. 다시 한 번, 이 로깅 API에서는 FastAPI를 활용하고 단일 엔드포인트를 사용하겠다. 따라서 새 디렉토리(원하는 이름으로 지정)와 새 main.py 파일을 만들고 다음 코드를 추가한다.

```python
from fastapi import Depends, FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel

from logger_repository import insert_new_log


app = FastAPI (
    title = "Logger",
    description = "",
    version = "1.0"
)
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"], # Allows all origins
    allow_credentials=True,
    allow_methods=["*"], # Allows all methods
    allow_headers=["*"], # Allows all headers
)

class LogRequest(BaseModel):
    log_type: str
    log_message: str

@app.get("/")
async def root():
    pass

@app.post("/add")
async def log_message(request: LogRequest):
    result = await insert_new_log(application_id, request)
    return result
```

대부분의 경우 위의 코드는 이전에 보지 못했던 것은 아닐 것이다. 유일하게 새롭게 추가된 것은 POST 요청을 수락하는 엔드포인트와 해당 엔드포인트로 전달되는 Pydantic 클래스 객체를 활용하는 것이다. Pydantic에 익숙하지 않은 분들을 위해 설명하자면, Python용 데이터 유효성 검사 라이브러리이다. 우리의 경우, 누군가 요청을 보내면 Pydantic은 자동으로 속성이 JSON 본문의 필드 이름과 일치하는지, 데이터 타입이 일치하는지 확인한다. 삶이 훨씬 쉬워진다! 이제 너무 흥분하지 마세요. 아직 끝나지 않았으니까요. 로그 레코드를 삽입할 함수가 아직 필요하다. 새 파일에 아래 코드를 추가한다.

```python
from logger_models import *

async def insert_new_log(new_log):
    try:
        with Session(engine) as session:
            inserted_log = Log()
            inserted_log.LogTypeId = results.LogTypeId
            inserted_log.LogText = new_log.log_message
            session.add(inserted_log)
            session.commit()
            return inserted_log
    except Exception as ex:
        data = {
            "message": str(ex)
        }
        return data
```

위의 코드를 살펴보면 작동 방식은 매우 간단하다. `LogRequest` 객체를 매개변수로 받는 단일 비동기 함수가 있다. 그런 다음 `try...except` 구문의 경계 내에서 `new_log` 객체를 데이터베이스에 삽입할 데이터베이스 객체로 변환하는 `SQLModel` 코드가 있다. 여기서는 SQLModel에 대해 자세히 설명하지 않겠다. 이 로깅 API에 필요한 Python 코드의 마지막 비트는 데이터베이스 객체 클래스를 보관하는 또 다른 파일(logger_models.py라고 함)이다.

```python
from typing import List, Optional
from sqlmodel import Field, Relationship, Session, SQLModel, create_engine, select

engine = create_engine("mysql+mysqlconnector://<USERNAME>:<PASSWORD>@<ADDRESS>:<PORT>/Logger", echo=True)
class Log(SQLModel, table=True):
    __tablename__ = "Log"
    LogId: Optional[int] = Field(default=None, primary_key=True)
    LogTypeId: str
    LogText: str
```

물론 실제 데이터베이스에 대해 이야기하지 않았기 때문에 아직 정확히 끝난 것은 아니다. 이 예에서는 MySQL 데이터베이스를 사용하고 있다. 이 데이터베이스는 설정이 쉽고 크로스 플랫폼이기 때문에 따라하는 사람들이 쉽게 사용할 수 있다. MySQL 서버를 설정하는 방법에 대해 자세히 설명하지는 않겠지만, 시작하는 데 도움이 될 만한 [정보](https://dev.mysql.com/doc/mysql-installation-excerpt/5.7/en/)를 제공한다. 서버가 실행 중이면 이 스크립트를 실행하여 데이터베이스와 로그 레코드 테이블을 생성하기만 하면 된다.

```sql
CREATE DATABASE Logger;

CREATE TABLE Log (
    LogId INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    LogTypeId VARCHAR(10) NOT NULL,
    LogText TEXT,
    LogCreatedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
```

거의 다 끝나간다! 첫 번째 API와 미들웨어 엔드포인트 내부로 돌아가서 이 마지막 코드를 추가한다.

```python
data = {
  "log_type": "INFO",
  "log_message": "A message."
}
   
add_log = requests.post(f"{<LOGGING_API_URL>}/add", json=data)
```

이제 드디어 테스트할 준비가 되었다. 아직 첫 번째 API를 실행하지 않았다면 지금 시작하자. 그런 다음 로깅 API를 시작하되, 다른 포트 번호를 사용해야 한다. 그렇지 않으면 시작되지 않고 경고 메시지가 표시된다. 포트 번호를 변경하려면 `--port` 명령어 옵션을 사용한다. 이제 남은 일은 첫 번째 API에서 요청을 보내는 것뿐이다. 모든 것이 정확히 작동했다면 데이터베이스에 로그 레코드가 표시되어야 한다. `mysql-connector-python`, `pydantic` 및 `sqlmodel`같은 몇 가지 Python 패키지를 설치해야 할 수도 있다. 이 부분은 연습이 완벽을 만들므로 여러분이 직접 처리하도록 하였다 ;).

## 마치며
요약하자면, FastAPI에서 로깅을 처리할 수 있는 세 가지 방법을 살펴보았다. 콘솔 로깅이 가장 쉽지만, 로그 레코드를 보는 것은 사용자 친화적이지 않다. 파일 로깅은 또 다른 훌륭한 로깅 방법이다. 앱의 로그 기록은 파일에 추가되며, 사용자는 해당 파일을 열고 원하는 모든 로깅을 읽기만 하면 된다. 이러한 파일을 검색하는 것은 다소 지루할 수 있으며, 쿼리에 더 적합한 것이 필요한 경우 데이터베이스 로깅을 사용하면 된다. 결국, 어떤 로깅 방법이 다른 방법보다 낫다고 할 수는 없다. 프로젝트에 필요한 것이 무엇이냐에 따라 달라질 뿐이다. 살펴본 위의 코드를 자유롭게 수정하고 발전시켜 보자. 결코 완벽하지도 않고 가장 안전한 방법도 아니지만, 새로운 것을 시도해보고 싶은 분들에게는 좋은 출발점이 될 것이다. 


<a name="footnote_1">1</a>: 이 페이지는 [FastAPI Logging in 2023](https://medium.com/codex/fastapi-logging-in-2023-f926fd0bdfcb)을 편역한 것임.
